terraform {
  required_providers {
    acme = {
      source  = "vancluever/acme"
      version = ">= 2.11"
    }

    pkcs12 = {
      source  = "chilicat/pkcs12"
      version = ">= 0.0.7"
    }
  }
}

provider "acme" {
  server_url = var.acme_server_url
}
