variable "acme_server_url" {
  type    = string
  default = "https://acme-v02.api.letsencrypt.org/directory"
}

variable "algorithm" {
  type    = string
  default = "RSA"

  validation {
    condition     = contains(["RSA", "ECDSA", "ED25519"], var.algorithm)
    error_message = "Valid values for var.algorithm are (RSA, ECDSA, ED25519)."
  }
}

variable "rsa_bits" {
  type    = number
  default = 2048
}

variable "email_address" {
  type    = string
  default = ""
}

variable "dns_names" {
  type = list(string)
}

variable "common_name" {
  type = string
}

variable "organization" {
  type    = string
  default = null
}

variable "organization_unit" {
  type    = string
  default = null
}

variable "country" {
  type    = string
  default = null
}

variable "gcp_dns_project" {
  type    = string
  default = null
}

variable "min_days_remaining" {
  type    = number
  default = 30
}


variable "enable_pkcs12" {
  type    = bool
  default = false
}

variable "pkcs12_password" {
  type    = string
  default = null
}
