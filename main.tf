locals {
  rsa_bits      = var.algorithm == "RSA" ? var.rsa_bits : null
  ecdsa_curve   = var.algorithm == "ECDSA" ? "P224" : null
  pkcs12_base64 = var.enable_pkcs12 ? pkcs12_from_pem.pkcs12[0].result : ""
}

resource "tls_private_key" "reg_private_key" {
  algorithm = "RSA"
}

resource "acme_registration" "reg" {
  account_key_pem = tls_private_key.reg_private_key.private_key_pem
  email_address   = var.email_address
}


resource "tls_private_key" "cert_private_key" {
  algorithm   = var.algorithm
  rsa_bits    = local.rsa_bits
  ecdsa_curve = local.ecdsa_curve
}

resource "tls_cert_request" "req" {
  private_key_pem = tls_private_key.cert_private_key.private_key_pem
  dns_names       = var.dns_names

  subject {
    common_name = var.common_name
  }
}

resource "acme_certificate" "certificate" {
  account_key_pem         = acme_registration.reg.account_key_pem
  certificate_request_pem = tls_cert_request.req.cert_request_pem
  min_days_remaining      = var.min_days_remaining

  dns_challenge {
    provider = "gcloud"
    config = {
      GCE_PROJECT = var.gcp_dns_project
    }
  }
}

resource "pkcs12_from_pem" "pkcs12" {
  count           = var.enable_pkcs12 ? 1 : 0
  password        = var.pkcs12_password
  cert_pem        = acme_certificate.certificate.certificate_pem
  private_key_pem = tls_private_key.cert_private_key.private_key_pem
  ca_pem          = acme_certificate.certificate.issuer_pem
}
