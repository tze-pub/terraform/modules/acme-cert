output "certificate_pem" {
  value = acme_certificate.certificate.certificate_pem
}

output "issuer_pem" {
  value = acme_certificate.certificate.issuer_pem
}

output "bundle_pem" {
  value = "${acme_certificate.certificate.certificate_pem}${acme_certificate.certificate.issuer_pem}"
}

output "domain" {
  value = acme_certificate.certificate.certificate_domain
}

output "pkcs12_base64" {
  value = local.pkcs12_base64
}

output "private_key_pem" {
  value     = tls_private_key.cert_private_key.private_key_pem
  sensitive = true
}
